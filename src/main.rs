use std::fmt;

fn main() {
    // hello();
    // primitives();
    // calc();
    // tuple();
    // array();
    // demo_struct();
    // demo_enum();
    // demo_linked_list();
    // demo_const();
    // demo_type();
    demo_type_alias();
}

fn hello() {
    println!("Hello, world!");
}

fn primitives() {
    let logical: bool = true;

    let a_float: f64 = 1.0;
    let b_float = 5i32;

    let default_float = 3.0;
    let default_integer = 7;

    let mut mutable = 12;
    mutable = 14;
    println!("{}", mutable);
}

fn calc() {
    println!("1 + 2 = {}", 1u32 + 2);
    println!("1 - 2 = {}", 1i32 - 2);

    println!("true and false is {}", true && false);
    println!("true or false is {}", true || false);
    println!("not true is {}", !true);

    println!("0011 AND 0101 is {:04b}", 0b0011u32 & 0b0101);
    println!("0011 OR 0101 is {:04b}", 0b0011u32 | 0b0101);
    println!("0011 XOR 0101 is {:04b}", 0b0011u32 ^ 0b0101);
    println!("1 << 5 is {}", 1u32 << 5);
    println!("0x80 >> 2 is 0x{:x}", 0x80u32 >> 2);

    println!("one million is written as {}", 1_000_000u32);
}

//       tuples
fn reverse(pair: (i32, bool)) -> (bool, i32) {
    let (i, b) = pair;
    (b, i)
}

fn transpose(mtx: Matrix) -> Matrix {
    let Matrix(a, b, c, d) = mtx;
    Matrix(a, c, b, d)
}

#[derive(Debug)]
struct Matrix(f32, f32, f32, f32);

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({} {})\n({} {})", self.0, self.1, self.2, self.3)
    }
}

fn tuple() {
    let long_tuple = (1u8, 2u16, 3u32, 4u64,
                      -1i8, -2i16, -3i32, -4i64,
                      0.1f32, 0.2f64,
                      true, 'a');
    println!("first of tuple is {}", long_tuple.0);
    println!("second of tuple is {}", long_tuple.1);

    let tuple_of_tuples = ((1u8, 2u32, 3u64), (4u64, -1i8), -2i16);

    let pair = (1, true);
    println!("pair is {:?}", pair);

    println!("the reversed pair is {:?}", reverse(pair));

    println!("tuple with only element is {:?}", (3u32,));
    println!("just one integer {:?}", (5u32));

    let _tuple = (1, "hello", 'h', 3.4, true);
    let (a, b, c, ..) = _tuple;
    println!("{:?} {:?} {:?}", a, b, c);

    let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    println!("matrix:\n{}", matrix);
    println!("transpose:\n{}", transpose(matrix));
}

//           array and slice
fn analyze_slice(slice: &[i32]) {
    println!("first element of slice is {}", slice[0]);
    println!("the slice {:?} has length {}", slice, slice.len());
}

fn array() {
    use std::mem;
    let xs: [i32; 5] = [1, 2, 3, 4, 5];
    let ys: [i32; 500] = [0; 500];

    println!("array occupies {} bytes", mem::size_of_val(&xs));
    println!("borrow the whole array as a slice");
    analyze_slice(&xs);
    analyze_slice(&ys[2..4]);
}

//         struct
struct Nil;
struct Pair(u32, u32);

struct Point {
    x: f64,
    y: f64,
}

#[allow(dead_code)]
struct Rectangle {
    p1: Point,
    p2: Point,
}

fn demo_struct() {
    let p: Point = Point{x: 0.3, y: 0.4};
    println!("point coordinates: ({}, {})", p.x, p.y);
    let Point{x: myx, y: myy} = p;
    let _rectangle = Rectangle {
        p1: Point {x: myx+3f64, y: myy+4f64},
        p2: p
    };

    let _nil = Nil;
    let pair = Pair(123, 25);
    println!("pair contains {} {}", pair.0, pair.1);
    println!("size of rectangle is {}", calc_size(&_rectangle));
}

fn calc_size(rec: &Rectangle) -> f64 {
    let Point{x: x1, y:y1} = rec.p1;
    let Point{x: x2, y:y2} = rec.p2;
    (x1 - x2).abs() * (y1 - y2).abs()
}

//                enum
enum Person {
    Engineer,
    Scientist,
    Height(u32),
    Weight(u32),
    Info {name: String, height: u32}
}

fn inspect_person(person: Person) {
    match person {
        Person::Engineer => println!("Is engineer!"),
        Person::Scientist => println!("Is scientist!"),
        Person::Height(i) => println!("Has height {}", i),
        Person::Weight(i) => println!("Has weight {}", i),
        Person::Info {name, height} => println!("{} is {} tall", name, height),
    }
}

fn demo_enum() {
    let person   = Person::Height(18);
    let amira    = Person::Weight(10);
    // `to_owned()` 从一个字符串 slice 创建一个具有所有权的 `String`。
    let dave     = Person::Info { name: "Dave".to_owned(), height: 72 };
    let rebecca  = Person::Scientist;
    let rohan    = Person::Engineer;

    inspect_person(person);
    inspect_person(amira);
    inspect_person(dave);
    inspect_person(rebecca);
    inspect_person(rohan);
}

// use 来展开 enum

//             linked-list
use List::*;

enum List {
    Cons(u32, Box<List>),
    None,
}

impl List {
    fn new() -> List {
        None
    }

    fn prepend(self, elem: u32) -> List {
        Cons(elem, Box::new(self))
    }

    fn len(&self) -> u32 {
        match *self {
            Cons(_, ref tail) => 1 + tail.len(),
            None => 0,
        }
    }

    fn stringify(&self) -> String {
        match *self {
            Cons(head, ref tail) => format!("{}, {}", head, tail.stringify()),
            None => format!("None"),
        }
    }
}

fn demo_linked_list() {
    let mut list = List::new();
    list = list.prepend(1);
    list = list.prepend(2);
    list = list.prepend(3);
    list = list.prepend(4);
    list = list.prepend(5);

    println!("linked list has length: {}", list.len());
    println!("{}", list.stringify());
}

fn demo_const() {
    static LANG: &'static str = "Rust";
    const THRESHOLD: i32 = 10;

    println!("static is a memory address: {}", LANG);
    println!("const represent a value: {}", THRESHOLD);
}


//            type transform
// as 用于数据类型安全转换；相反，transmute 允许类型之间的任意转换
fn demo_type() {
    let decimal = 64.4321f32;
    // not allowed
    //let integer: u8 = decimal;

    let integer = decimal as u8;
    let character = integer as char;

    println!("casting: {} -> {} -> {}", decimal, integer, character);

    // 当将任意整数值转成无符号类型（unsigned 类型） T 时，
    // 将会加上或减去 std::T::MAX + 1，直到值符合新的类型
    println!("1000 as a u16 is: {}", 1000 as u16);
    // 二进制截取低位 py: 1000 & 0b11111111
    println!("1000 as a u8 is {}", 1000 as u8);
    // py: -1 & 0b11111111
    println!("-1 as a u8 is {}", (-1i8) as u8);


    // 对正数来说，上面的类型转换操作和取模效果一样
    println!("1000 mod 256 is: {}", 1000 % 256);

    // 当将整数值转成有符号类型（signed 类型）时，同样要先将（二进制）数值
    // 转成相应的无符号类型（译注：如 i32 和 u32 对应，i16 和 u16对应），
    // 然后再求此值的补码（two's complement）。如果数值的最高位是 1，则数值
    // 是负数。
    println!("128 as a i16 is {}", 128 as i16);
    println!("128 as a i8 is {}", 128 as i8);

    println!("1000 as a i8 is {}", 1000 as i8);
    // 232 的补码是 -24
    println!("232 as a i8 is {}", 232 as i8);
}

fn demo_type_alias() {
    // `NanoSecond` 是 `u64` 的新名字。
    type NanoSecond = u64;
    type Inch = u64;

    // 使用一个属性来忽略警告。
    #[allow(non_camel_case_types)]
    type u64_t = u64;
    // 试一试 ^ 试着删掉属性。

    // `NanoSecond` = `Inch` = `u64_t` = `u64`.
    let nanoseconds: NanoSecond = 5 as u64_t;
    let inches: Inch = 2 as u64_t;

    // 注意类型的别名*没有*提供任何额外的类型安全，因为别名*不是*新的类型
    println!("{} nanoseconds + {} inches = {} unit?",
             nanoseconds,
             inches,
             nanoseconds + inches);
}